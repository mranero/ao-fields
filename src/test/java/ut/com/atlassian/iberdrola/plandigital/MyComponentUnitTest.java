package ut.com.atlassian.iberdrola.plandigital;

import org.junit.Test;
import com.atlassian.iberdrola.plandigital.api.MyPluginComponent;
import com.atlassian.iberdrola.plandigital.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}