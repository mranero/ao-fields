package com.atlassian.iberdrola.plandigital;

import net.java.ao.Entity;

public interface BudgetOwner extends Entity
{
    String getDescription();

    void setDescription(String description);

    boolean isComplete();

    void setComplete(boolean complete);
}