package com.atlassian.iberdrola.plandigital;

import com.atlassian.activeobjects.tx.Transactional;

import java.util.List;

@Transactional
public interface BudgetOwnerService
{
    BudgetOwner add(String description);
    void del(String description);    

    List<BudgetOwner> all();
}