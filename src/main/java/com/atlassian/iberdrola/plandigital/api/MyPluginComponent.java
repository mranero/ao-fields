package com.atlassian.iberdrola.plandigital.api;

public interface MyPluginComponent
{
    String getName();
}