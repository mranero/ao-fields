package com.atlassian.iberdrola.plandigital;

import com.atlassian.activeobjects.tx.Transactional;

import java.util.List;

@Transactional
public interface TechnologyService
{
    Technology add(String description);
    void del(String description);    

    List<Technology> all();
}