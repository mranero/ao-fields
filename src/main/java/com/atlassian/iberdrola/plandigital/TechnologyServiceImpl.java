package com.atlassian.iberdrola.plandigital;

import com.atlassian.activeobjects.external.ActiveObjects;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import javax.inject.Inject;
import javax.inject.Named;

@Scanned
@Named
public class TechnologyServiceImpl implements TechnologyService
{
    @ComponentImport
    private final ActiveObjects ao;

    @Inject
    public TechnologyServiceImpl(ActiveObjects ao)
    {
        this.ao = checkNotNull(ao);
    }

    @Override
    public Technology add(String description)
    {
        final Technology todo = ao.create(Technology.class);
        todo.setDescription(description);
        todo.setComplete(false);
        todo.save();
        return todo;
    }
    @Override
    public void del(String description)
    {
        ao.deleteWithSQL(Technology.class, "DESCRIPTION like ?", description);
    }
    @Override
    public List<Technology> all()
    {
        return newArrayList(ao.find(Technology.class));
    }
}