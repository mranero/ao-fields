package com.atlassian.iberdrola.plandigital;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import com.atlassian.templaterenderer.TemplateRenderer;
import javax.inject.Inject;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import static com.google.common.base.Preconditions.*;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
@Scanned
public final class BudgetOwnerServlet extends HttpServlet
{
    //private static final Logger log = LoggerFactory.getLogger(BudgetOwnerServlet.class);    
    @ComponentImport
    private final TemplateRenderer templateRenderer;  
    private final BudgetOwnerService todoService;
    @Inject
    public BudgetOwnerServlet(BudgetOwnerService todoService, TemplateRenderer templateRenderer)    
    {
        this.todoService = checkNotNull(todoService);
        this.templateRenderer = checkNotNull(templateRenderer);        
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {         
      String act = (String) req.getParameter("action");
      String des = (String) req.getParameter("description");     
      if (act!=null)
        if (act.equals("delete")){         
            todoService.del(des);
        }
      Map<String, Object> context = new HashMap<String, Object>();   
      
      ArrayList<String> values= new ArrayList<String>();
      for (BudgetOwner todo : todoService.all())
      {
          values.add(todo.getDescription());
      }
      context.put("values",values);
      res.setContentType("text/html;charset=utf-8");
      templateRenderer.render("BudgetOwner.vm", context, res.getWriter());        
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        final String description = req.getParameter("task");
        todoService.add(description);
        res.sendRedirect(req.getContextPath() + "/plugins/servlet/bo/list");
    }
}