package com.atlassian.iberdrola.plandigital;

import com.atlassian.activeobjects.external.ActiveObjects;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import javax.inject.Inject;
import javax.inject.Named;

@Scanned
@Named
public class BudgetOwnerServiceImpl implements BudgetOwnerService
{
    @ComponentImport
    private final ActiveObjects ao;

    @Inject
    public BudgetOwnerServiceImpl(ActiveObjects ao)
    {
        this.ao = checkNotNull(ao);
    }

    @Override
    public BudgetOwner add(String description)
    {
        final BudgetOwner todo = ao.create(BudgetOwner.class);
        todo.setDescription(description);
        todo.setComplete(false);
        todo.save();
        return todo;
    }
    @Override
    public void del(String description)
    {
        ao.deleteWithSQL(BudgetOwner.class, "DESCRIPTION like ?", description);
    }
    @Override
    public List<BudgetOwner> all()
    {
        return newArrayList(ao.find(BudgetOwner.class));
    }
}